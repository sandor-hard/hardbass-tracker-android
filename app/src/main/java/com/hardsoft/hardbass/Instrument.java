package com.hardsoft.hardbass;

import java.io.Serializable;

public class Instrument implements Serializable {

    private static final long serialVersionUID = 4424875519683159536L;

    private final int mIndex;

    private int mHoldGateForFrames = -1; //When >0, overrides the track gate; Set to -1 if not used

    private LFO mVibrato = new LFO(true, 0x60, 8, 8);
    private LFO mCutoffLFO = new LFO(true, 3, 8, 8);
    private LFO mPwLFO = new LFO(true, 0x20, 8, 8);

    public Instrument(int index) {
        mIndex = index;
    }

    public int getIndex() {
        return mIndex;
    }

    public int getHoldGateForFrames() {
        return mHoldGateForFrames;
    }

    public double getVibratoDepthRangeMultiplier() {
        final float vibratoDepthInSeminotes = (float)mVibrato.depth / 255f;
        return Math.pow(HBEngine.NOTE_MULT, vibratoDepthInSeminotes);
    }

    public boolean getVibratoEnabled() {
        return mVibrato.enabled;
    }

    public int getVibratoDelayFrames() {
        return mVibrato.delayFrames;
    }

    public int getVibratoPeriodFrames() {
        return mVibrato.periodFrames;
    }

    public int getCutoffLFODepth() {
        return mCutoffLFO.depth;
    }

    public boolean getCutoffLFOEnabled() {
        return mCutoffLFO.enabled;
    }

    public int getCutoffLFODelayFrames() {
        return mCutoffLFO.delayFrames;
    }

    public int getCutoffLFOPeriodFrames() {
        return mCutoffLFO.periodFrames;
    }


    public int getPwLFODepth() {
        return mPwLFO.depth;
    }

    public boolean getPwLFOEnabled() {
        return mPwLFO.enabled;
    }

    public int getPwLFODelayFrames() {
        return mPwLFO.delayFrames;
    }

    public int getPwLFOPeriodFrames() {
        return mPwLFO.periodFrames;
    }

    public LFO getVibrato() {
        return mVibrato;
    }

    public LFO getCutoffLFO() {
        return mCutoffLFO;
    }

    public LFO getPwLFO() {
        return mPwLFO;
    }

    public void setHoldGateForFrames(int frames) {
        mHoldGateForFrames = frames;
    }
}
