package com.hardsoft.hardbass;

import android.app.Activity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

public abstract class LFOUi {

    protected final HBEngine mHBEngine;
    protected final View mInstrumentEditor;
    protected final Activity mActivity;

    protected final ToggleButton mEnabledToggle;
    protected final NumberIncDec mDepthIncDec;
    protected final NumberIncDec mDelayIncDec;
    protected final NumberIncDec mPeriodIncDec;

    public LFOUi(Activity activity, HBEngine hbEngine, View instrumentEditor,
                 int enabledResId, int depthResId, int delayResId, int periodResId) {
        mActivity = activity;
        mHBEngine = hbEngine;
        mInstrumentEditor = instrumentEditor;

        mEnabledToggle = (ToggleButton) activity.findViewById(enabledResId);
        mEnabledToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getLFO().enabled = isChecked;
            }
        });

        final TextView depth = (TextView) activity.findViewById(depthResId);
        mDepthIncDec = new NumberIncDec(depth) {
            @Override
            public boolean onInc(int newNum) {
                getLFO().depth = newNum;
                return true;
            }
        };
        depth.setTag(mDepthIncDec);

        final TextView delay = (TextView) activity.findViewById(delayResId);
        mDelayIncDec = new NumberIncDec(delay) {
            @Override
            public boolean onInc(int newNum) {
                getLFO().delayFrames = newNum;
                return true;
            }
        };
        delay.setTag(mDelayIncDec);

        final TextView period = (TextView) activity.findViewById(periodResId);
        mPeriodIncDec = new NumberIncDec(period) {
            @Override
            public boolean onInc(int newNum) {
                final boolean result = newNum>=3;
                if (result) {
                    getLFO().periodFrames = newNum;
                }
                return result;
            }
        };
        period.setTag(mPeriodIncDec);

    }

    protected Instrument getInstrument() {
        final PatternRow row = (PatternRow) mInstrumentEditor.getTag();
        return mHBEngine.getSong().instruments[row.getInstrument()];
    }

    protected abstract LFO getLFO();

    public void updateUi() {
        final LFO lfo = getLFO();
        mEnabledToggle.setChecked(lfo.enabled);
        mDepthIncDec.setNum(lfo.depth);
        mDelayIncDec.setNum(lfo.delayFrames);
        mPeriodIncDec.setNum(lfo.periodFrames);
    }

}
