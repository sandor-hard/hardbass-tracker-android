package com.hardsoft.hardbass;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ExportCommands {

    private static final String TAG = "ExportCommands";

    private final int mIndex;
    private final ByteArrayOutputStream mOutputStream = new ByteArrayOutputStream();
    private byte[] mBytes = null;
    private String mChecksum = null;
    private boolean mIsEnded = false;
    private byte[] mLastCommand = null;

    private int mAddressInExport = -1;

    public ExportCommands(int index) {
        mIndex = index;
    }

    public void addCommand(byte... b) {
        if (!mIsEnded) {
            try {
                if (mLastCommand!=null) {
                    mOutputStream.write(mLastCommand);
                }
                mLastCommand = b;
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException("Can not save export in memory");
            }
        }
    }

    public void end() {
        if (!mIsEnded) {
            mIsEnded = true;

            commitLastCommand();

            mBytes = mOutputStream.toByteArray();

            final MessageDigest m;
            try {
                m = MessageDigest.getInstance("SHA1");
                m.update(mBytes);
                mChecksum = String.format("%x", new BigInteger(1, m.digest()));

                Log.d(TAG, String.format("Checksum %d = %s (len:%x)", mIndex, mChecksum, mBytes.length));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                throw new RuntimeException("Can not calculate export checksum");
            }

            try {
                mOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean hasLastCommand() {
        return mLastCommand!=null;
    }

    public void commitLastCommand() {
        if (mLastCommand!=null) {
            try {
                mOutputStream.write(mLastCommand);
                mLastCommand = null;
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException("Can not save last export in memory");
            }
        }
    }

    public void addSkipFramesToLastCommand(byte skipFrames) {
        if (mLastCommand!=null) {
            mLastCommand[0]|= skipFrames;
        }
    }

    public byte[] getBytes() {
        return mBytes;
    }

    public String getChecksum() {
        return mChecksum;
    }

    public void setAddressInExport(int address) {
        mAddressInExport = address;
    }

    public int getAddressInExport() {
        return mAddressInExport;
    }
}
