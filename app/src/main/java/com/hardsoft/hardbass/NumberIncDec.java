package com.hardsoft.hardbass;

import android.widget.TextView;

public abstract class NumberIncDec {

    private final TextView mTextView;

    private int mNum = 0;

    protected NumberIncDec(TextView textView) {
        mTextView = textView;
        setText();
    }

    private void setText() {
        final String numStr = (mNum < 0x10 ? "0" : "") + Integer.toHexString(mNum).toUpperCase();
        mTextView.setText(numStr);
    }

    public int getNum() {
        return mNum;
    }

    public void setNum(int num) {
        mNum = num;
        setText();
    }

    public void incNum(int inc) {
        if (mNum+inc>=0) {
            final int newNum = (mNum + inc) & 0xff;
            setNewNumIfListenerWillTakeIt(newNum);
        }
    }

    public abstract boolean onInc(int newNum);

    public void invert() {
        final int newNum = 255-mNum;
        setNewNumIfListenerWillTakeIt(newNum);
    }

    private void setNewNumIfListenerWillTakeIt(int newNum) {
        if (callOnInc(newNum)) {
            mNum = newNum;
            setText();
        }
    }

    private boolean callOnInc(int newNum) {
        boolean result = false;

        try {
            result = onInc(newNum);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

}
