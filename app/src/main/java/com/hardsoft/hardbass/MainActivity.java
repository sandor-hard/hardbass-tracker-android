/*
    HARDBASS Tracker
    to be used with HARDBASS Atari XL/XE as introduced in the REHARDEN demo

    Created by Sandor Teli / HARD (10/2016-11/2017)

    Experimental/alpha code aiming at quick results
    Feel free to improve/rewrite the Tracker UI
 */

package com.hardsoft.hardbass;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_EXPORT = 1;
    private static final int PERMISSION_REQUEST_LOAD = 2;
    private static final int PERMISSION_REQUEST_SAVE = 3;
    private static final int PERMISSION_REQUEST_BACKUP = 4;
    private static final int PERMISSION_REQUEST_IMPORT_RMT_TRACK = 5;

    int sampleRate;
    int bufSize;
    private AudioOutput audioOutput;
    private short[] samples;
    private int[] aSamlples;

    private float aToRealFreqRatio;
    private int upscaledSampleBuffSize;
    private int numABuffsInBuff;

    HBEngine mHBEngine;

    private ListView mPatternView;
    private int mPatternViewHeight = -1;
    private int mPatternViewItemHeight = -1;
    private int mPatternViewCenterItemOffset = -1;
    private int mTrackerStepCursor = -1;
    private Pattern mPattern = null;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private PatternAdapter mPatternAdapter;

    private RecyclerView mSongPatternView;
    private SongPatternAdapter mSongPatternAdapter;

    private View mToolbarHeader;
    private int mToolbarHeaderHeight = -1;

    private TextView mSongLenghtView;
    private TextView mSongLoopView;

    private LFOUi mVibratoUi;
    private LFOUi mCutoffLFOUi;
    private LFOUi mPwLFOUi;

    private boolean isExportingOrImporting = false;
    private boolean mIsInitialSongLoadingFinished = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mHBEngine = new HBEngine();

        setupUi();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            AudioManager myAudioMgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            String nativeParam = myAudioMgr.getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE);
            sampleRate = Integer.parseInt(nativeParam);
            nativeParam = myAudioMgr.getProperty(AudioManager.PROPERTY_OUTPUT_FRAMES_PER_BUFFER);
            bufSize = Integer.parseInt(nativeParam);
        }
        else {
            sampleRate = 44100;
            bufSize = AudioTrack.getMinBufferSize(sampleRate,
                    AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
        }

        aSamlples = new int[HBEngine.BUFF_SIZE];
        aToRealFreqRatio = sampleRate / (float) HBEngine.SAMPLE_RATE;
        upscaledSampleBuffSize = (int) (aSamlples.length * aToRealFreqRatio);

        numABuffsInBuff = bufSize/upscaledSampleBuffSize+1;
        bufSize = numABuffsInBuff*upscaledSampleBuffSize;

        audioOutput = new AudioOutput();
        audioOutput.Start(sampleRate, bufSize);


        samples = new short[bufSize];

        new PlayTask().execute();
        mHandler.post(createFollowTrackerRunnable());
    }

    private void setupUi() {
        mPatternView = (ListView) findViewById(R.id.patternView);
        mPatternView.setDivider(null);

        final View.OnClickListener patternRowClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view!=null && view.getTag() instanceof PatternRow) {
                    final int id = view.getId();
                    switch (id) {
                        case R.id.patternRowNote:
                            showNoteSelector(view, (PatternRow) view.getTag());
                            break;
                        case R.id.patternRowInstr:
                            showInstrumentSelector(view, (PatternRow) view.getTag());
                            break;
                        case R.id.patternGateCmd:
                            cycleGateCmdInRow(view, (PatternRow) view.getTag());
                            break;
                        case R.id.patternRowCutoff:
                            showDigitSelector(view, (PatternRow) view.getTag());
                            break;
                        case R.id.patternRowPW:
                            showDigitSelector(view, (PatternRow) view.getTag());
                            break;
                        default:
                            break;
                    }
                }
            }
        };

        mPatternAdapter = createPatternAdapter(patternRowClickListener);
        mPatternView.setAdapter(mPatternAdapter);

        ((ToggleButton) findViewById(R.id.play)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mHBEngine.resetPlaybackPosition(false);
                mHBEngine.setSongPlaying(b);
                findViewById(R.id.patternRowHighlight).setVisibility(b?View.VISIBLE:View.GONE);
            }
        });

        ((Button) findViewById(R.id.export)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mHBEngine.resetPlaybackPosition(true);
                mHBEngine.setSongPlaying(false);
                findViewById(R.id.patternRowHighlight).setVisibility(View.GONE);

                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    doExport();
                }
                else {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSION_REQUEST_EXPORT);
                }

            }
        });

        ((Button) findViewById(R.id.import_rmt)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mHBEngine.setSongPlaying(false);
                findViewById(R.id.patternRowHighlight).setVisibility(View.GONE);

                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    doImportRmtTrack();
                }
                else {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSION_REQUEST_IMPORT_RMT_TRACK);
                }

            }
        });

        ((Button) findViewById(R.id.backup)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mIsInitialSongLoadingFinished) {
                    stopSongPlayback();

                    if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        doBackup();
                    } else {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                PERMISSION_REQUEST_BACKUP);
                    }
                }
            }
        });

        mSongPatternView = (RecyclerView) findViewById(R.id.song_pattern_view);
        mSongPatternAdapter = new SongPatternAdapter(mHBEngine, getLayoutInflater(),
                new SongPatternAdapter.NumIncListener() {
                    @Override
                    public void onInc(int position, int newNum) {
                        stopSongPlayback();
                        closeAllWindows();
                        final Song song = mHBEngine.getSong();
                        song.makePatternAvailable(newNum);
                        song.patternIndices[position] = newNum;
                        setCurrentPattern(position, mHBEngine.setCurrentPattern(position, newNum));
                        mPatternAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onClick(int position, int p) {
                        stopSongPlayback();
                        closeAllWindows();
                        setCurrentPattern(position, mHBEngine.setCurrentPattern(position, p));
                        mPatternAdapter.notifyDataSetChanged();
                    }
                });

        final LinearLayoutManager lm = new LinearLayoutManager(
                MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        mSongPatternView.setLayoutManager(lm);

        mSongPatternView.setAdapter(mSongPatternAdapter);

        final ListView noteList = (ListView) findViewById(R.id.noteSelectorList);
        noteList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.selector_item, PatternRow.NOTE_NAMES));
        noteList.setDivider(null);

        final String[] octaves = new String[HBEngine.OCTAVES];
        for (int o=0;o<octaves.length;o++) {
            octaves[o] = String.valueOf(o);
        }

        final ListView octList = (ListView) findViewById(R.id.octaveSelectorList);
        octList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.selector_item, octaves));
        octList.setDivider(null);

        final String[] instruments = new String[0x100];
        for (int i=0;i<0x100;i++) {
            instruments[i] = (i<0x10?"0":"")+Integer.toHexString(i);
        }

        final ListView instrumentList = (ListView) findViewById(R.id.instrumentSelectorList);
        instrumentList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.selector_item, instruments));
        instrumentList.setDivider(null);

        final String[] digits = new String[0x10];
        for (int d=0;d<0x10;d++) {
            digits[d] = Integer.toHexString(d);
        }

        final ListView digitList = (ListView) findViewById(R.id.digitSelectorList);
        digitList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.selector_item, digits));
        digitList.setDivider(null);

        findViewById(R.id.clearNote).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PatternRow row = (PatternRow) mPatternAdapter.getItem(
                        mPatternView.getCheckedItemPosition() - PatternAdapter.PADDING_TRACK_ROWS);
                row.setNote(-1);
                row.setInstrument(-1);
                mPatternAdapter.updateRow(row);
            }
        });

        findViewById(R.id.clearNumber).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View selector = findViewById(R.id.digitSelector);
                final View editedView = (View) selector.getTag();
                final PatternRow row = (PatternRow) editedView.getTag();

                switch (editedView.getId()) {
                    case R.id.patternRowCutoff:
                        row.setCutOff(-1);
                        break;
                    case R.id.patternRowPW:
                        row.setPW(-1);
                        break;
                    default:
                        break;
                }

                mPatternAdapter.updateRow(row);
            }
        });

        findViewById(R.id.clearInstrument).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PatternRow row = (PatternRow) mPatternAdapter.getItem(
                        mPatternView.getCheckedItemPosition() - PatternAdapter.PADDING_TRACK_ROWS);
                row.setInstrument(-1);
                mPatternAdapter.updateRow(row);
            }
        });

        findViewById(R.id.editInstrument).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PatternRow row = (PatternRow) mPatternAdapter.getItem(
                        mPatternView.getCheckedItemPosition() - PatternAdapter.PADDING_TRACK_ROWS);
                showInstrumentEditor(row);
            }
        });

        findViewById(R.id.closeNoteSelector).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeNoteSelector();
            }
        });

        findViewById(R.id.closeDigitSelector).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDigitSelector();
            }
        });

        findViewById(R.id.closeInstrumentSelector).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeInstrumentSelector();
            }
        });

        octList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                setNoteInRow();
            }
        });

        noteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                setNoteInRow();
            }
        });

        instrumentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                setInstrumentInRow();
            }
        });

        digitList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                enterDigitInRow();
            }
        });

        mToolbarHeader = findViewById(R.id.toolbar_header);

        final TextView ninc = (TextView) findViewById(R.id.numberinc);
        final TextView ndec = (TextView) findViewById(R.id.numberdec);
        final TextView nincIns = (TextView) findViewById(R.id.numberincins);
        final TextView ndecIns = (TextView) findViewById(R.id.numberdecins);
        final TextView ninvIns = (TextView) findViewById(R.id.numberinvins);
        ndec.setText("-");
        ndecIns.setText("-");
        ninvIns.setText("i");

        mSongLenghtView = (TextView) findViewById(R.id.song_lenght);
        mSongLoopView = (TextView) findViewById(R.id.song_loop);

        mSongLenghtView.setTag(new NumberIncDec(mSongLenghtView) {
            @Override
            public boolean onInc(int newNum) {
                stopSongPlayback();

                final boolean result = newNum>0;

                if (result) {
                    mHBEngine.getSong().setLength(newNum);
                    mPatternAdapter.notifyDataSetChanged();
                    mSongPatternAdapter.notifyDataSetChanged();

                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            updateSongDisplay();
                        }
                    });
                }

                return result;
            }
        });

        mSongLoopView.setTag(new NumberIncDec(mSongLoopView) {
            @Override
            public boolean onInc(int newNum) {
                final boolean result = newNum < mHBEngine.getSong().length;
                if (result) {
                    stopSongPlayback();
                    mHBEngine.getSong().loopPos = newNum;
                }
                return result;
            }
        });

        final View.OnClickListener nincClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incNum(1);
            }
        };
        ninc.setOnClickListener(nincClick);
        nincIns.setOnClickListener(nincClick);

        final View.OnClickListener ndecClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incNum(-1);
            }
        };
        ndec.setOnClickListener(ndecClick);
        ndecIns.setOnClickListener(ndecClick);

        ninvIns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invNum();
            }
        });

        final TextView insHoldGateForFramesView = (TextView) findViewById(R.id.insholdgateframes);

        insHoldGateForFramesView.setTag(new NumberIncDec(insHoldGateForFramesView) {
            @Override
            public boolean onInc(int newNum) {
                final int setting = (newNum>0)?newNum:-1;
                getInstrumentInSelectedRow().setHoldGateForFrames(setting);
                return true;
            }
        });

        final View editor = findViewById(R.id.instrumentEditor);
        mVibratoUi = new LFOUi(this, mHBEngine, editor, R.id.insvibena, R.id.insvibdepth, R.id.insvibdelayframes,
                R.id.insvibperiodframes) {

            @Override
            protected LFO getLFO() {
                return getInstrument().getVibrato();
            }
        };

        mCutoffLFOUi = new LFOUi(this, mHBEngine, editor, R.id.insclfoena, R.id.insclfodepth, R.id.insclfodelayframes,
                R.id.insclfoperiodframes) {

            @Override
            protected LFO getLFO() {
                return getInstrument().getCutoffLFO();
            }
        };

        mPwLFOUi = new LFOUi(this, mHBEngine, editor, R.id.inspwlfoena, R.id.inspwlfodepth, R.id.inspwlfodelayframes,
                R.id.inspwlfoperiodframes) {

            @Override
            protected LFO getLFO() {
                return getInstrument().getPwLFO();
            }
        };

        updateSongDisplay();
    }

    protected Instrument getInstrumentInSelectedRow() {
        final View editor = findViewById(R.id.instrumentEditor);
        final PatternRow row = (PatternRow) editor.getTag();
        return mHBEngine.getSong().instruments[row.getInstrument()];
    }

    private void stopSongPlayback() {
        mHBEngine.resetPlaybackPosition(false);
        mHBEngine.setSongPlaying(false);
    }

    private void incNum(int inc) {
        final View f = getCurrentFocus();
        if (f!=null && f.getTag() instanceof NumberIncDec) {
            ((NumberIncDec) f.getTag()).incNum(inc);
        }
    }

    private void invNum() {
        final View f = getCurrentFocus();
        if (f!=null && f.getTag() instanceof NumberIncDec) {
            final NumberIncDec incdec = (NumberIncDec) f.getTag();
            incdec.invert();
        }
    }

    private void enterDigitInRow() {
        final View selector = findViewById(R.id.digitSelector);
        final View editedView = (View) selector.getTag();
        final PatternRow row = (PatternRow) editedView.getTag();
        final ListView list = (ListView) findViewById(R.id.digitSelectorList);
        final int i = list.getCheckedItemPosition();
        switch (editedView.getId()) {
            case R.id.patternRowCutoff:
                row.setCutOff(i);
                break;
            case R.id.patternRowPW:
                final int currentValue = row.getPW();
                if (currentValue<0) {
                    row.setPW(i);
                }
                else {
                    row.setPW(((currentValue<<4)&0xf0)|i);
                }
                break;
            default:
                break;
        }
        mPatternAdapter.updateRow(row);
    }

    private void cycleGateCmdInRow(View view, PatternRow row) {
        row.cycleGateCmd();
        mPatternAdapter.updateRow(row);
    }

    private void setInstrumentInRow() {
        final View selector = findViewById(R.id.instrumentSelector);
        final PatternRow row = (PatternRow) selector.getTag();
        final ListView list = (ListView) findViewById(R.id.instrumentSelectorList);
        final int i = list.getCheckedItemPosition();
        mHBEngine.getSong().makeInstrumentAvailable(i);
        row.setInstrument(i);
        mPatternAdapter.updateRow(row);
    }

    private void setNoteInRow() {
        final View selector = findViewById(R.id.noteSelector);
        final PatternRow row = (PatternRow) selector.getTag();
        final ListView noteList = (ListView) findViewById(R.id.noteSelectorList);
        final ListView octList = (ListView) findViewById(R.id.octaveSelectorList);
        final int oct = octList.getCheckedItemPosition();
        final int note = noteList.getCheckedItemPosition();
        row.setNote(oct*12+note);
        mPatternAdapter.updateRow(row);
    }

    private void showNoteSelector(View view, PatternRow row) {
        closeAllWindows();

        final View noteSelector = findViewById(R.id.noteSelector);
        noteSelector.setTag(row);
        noteSelector.setVisibility(View.VISIBLE);
        final ListView noteList = (ListView) findViewById(R.id.noteSelectorList);
        final ListView octList = (ListView) findViewById(R.id.octaveSelectorList);

        if (row.getNote()>=0) {
            octList.setItemChecked(row.getNote() / 12, true);
            noteList.setItemChecked(row.getNote()%12, true);
        }
        else {
            octList.setItemChecked(0, true);
            noteList.setItemChecked(0, true);
        }

        mPatternView.setItemChecked(row.getIndex() + PatternAdapter.PADDING_TRACK_ROWS, true);
    }

    private void closeAllWindows() {
        closeNoteSelector();
        closeInstrumentSelector();
        closeDigitSelector();
        closeInstrumentEditor();
    }

    private void closeInstrumentEditor() {
        final View editor = findViewById(R.id.instrumentEditor);
        editor.setVisibility(View.GONE);
    }

    private void closeDigitSelector() {
        final View noteSelector = findViewById(R.id.digitSelector);
        noteSelector.setVisibility(View.GONE);
    }

    private void closeNoteSelector() {
        final View noteSelector = findViewById(R.id.noteSelector);
        noteSelector.setVisibility(View.GONE);
    }

    private void closeInstrumentSelector() {
        final View selector = findViewById(R.id.instrumentSelector);
        selector.setVisibility(View.GONE);
    }

    private void showInstrumentSelector(View view, PatternRow row) {
        closeAllWindows();

        final View selector = findViewById(R.id.instrumentSelector);
        selector.setTag(row);
        selector.setVisibility(View.VISIBLE);
        final ListView list = (ListView) findViewById(R.id.instrumentSelectorList);

        if (row.getInstrument()>=0) {
            list.setItemChecked(row.getInstrument(), true);
        }
        else {
            list.setItemChecked(0, true);
        }

        mPatternView.setItemChecked(row.getIndex() + PatternAdapter.PADDING_TRACK_ROWS, true);
    }

    private void showDigitSelector(View view, PatternRow row) {
        closeAllWindows();

        final View selector = findViewById(R.id.digitSelector);
        selector.setTag(view);
        selector.setVisibility(View.VISIBLE);
        final ListView list = (ListView) findViewById(R.id.digitSelectorList);

        int currentValue = -1;
        switch (view.getId()) {
            case R.id.patternRowCutoff:
                currentValue = row.getCutOff();
                break;
            case R.id.patternRowPW:
                currentValue = row.getPW()&0xf;
                break;
            default:
                break;
        }

        if (currentValue>=0) {
            list.setItemChecked(currentValue, true);
        }

        mPatternView.setItemChecked(row.getIndex() + PatternAdapter.PADDING_TRACK_ROWS, true);
    }

    private void showInstrumentEditor(PatternRow row) {
        if (row.getInstrument()>=0) {
            closeAllWindows();

            final View editor = findViewById(R.id.instrumentEditor);
            editor.setTag(row);
            editor.setVisibility(View.VISIBLE);

            mVibratoUi.updateUi();
            mCutoffLFOUi.updateUi();
            mPwLFOUi.updateUi();

            final Instrument instrument = mHBEngine.getSong().instruments[row.getInstrument()];
            final int holdGateForFramesSetting = instrument.getHoldGateForFrames();
            final boolean isHoldGateEnabled = holdGateForFramesSetting > 0;
            final View holdGateFramesView = findViewById(R.id.insholdgateframes);
            ((NumberIncDec) holdGateFramesView.getTag())
                    .setNum(isHoldGateEnabled?holdGateForFramesSetting:0);

            holdGateFramesView.requestFocus();
        }
    }

    private Runnable createFollowTrackerRunnable() {
        return new Runnable() {
            @Override
            public void run() {
                if (!isExportingOrImporting) {
                    final Pattern pattern = mHBEngine.getCurrentPattern();
                    if (mPattern != pattern) {
                        mTrackerStepCursor = -1;

                        setCurrentPattern(mHBEngine.getSongCursor(), pattern);

                        mPatternAdapter.notifyDataSetChanged();
                    }
                    else {
                        updatePatternNumbers(mHBEngine.getSongCursor(), mPattern);
                    }

                    final int trackerStepCursor = mHBEngine.getTrackerStepCursor();
                    if (mTrackerStepCursor != trackerStepCursor
                            && mPatternView.getChildAt(0) != null) {
                        if (mPatternViewHeight < 1) {
                            mPatternViewHeight = mPatternView.getHeight();
                            mPatternViewItemHeight = mPatternView.getChildAt(0).getHeight();
                            mPatternViewCenterItemOffset = mPatternViewHeight / mPatternViewItemHeight / 2;

                            mToolbarHeaderHeight = mToolbarHeader.getHeight();

                            final View hightlight = findViewById(R.id.patternRowHighlight);

                            final RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(
                                    hightlight.getLayoutParams());

                            p.setMargins(
                                    0, mToolbarHeaderHeight + mPatternViewCenterItemOffset * mPatternViewItemHeight, 0, 0);
                            hightlight.setLayoutParams(p);
                        }

                        mPatternView.setSelection(PatternAdapter.PADDING_TRACK_ROWS - mPatternViewCenterItemOffset
                                + trackerStepCursor);

                        mTrackerStepCursor = trackerStepCursor;
                    }
                }

                mHandler.postDelayed(this, 80);
            }
        };
    }

    @NonNull
    private PatternAdapter createPatternAdapter(View.OnClickListener patternRowClickListener) {
        return new PatternAdapter(mHBEngine, getLayoutInflater(), patternRowClickListener);
    }

    public void setCurrentPattern(int songPosition, Pattern p) {
        mPattern = p;
        updatePatternNumbers(songPosition, p);
    }

    private void updatePatternNumbers(int songPosition, Pattern p) {
        ((TextView)findViewById(R.id.pattern_num)).setText(String.format("S%s/P%s",
                getTwoDigitHex(songPosition), getTwoDigitHex(p.getIndex())));
    }

    String getTwoDigitHex(int num) {
        final String numStr = (num < 0x10 ? "0" : "") + Integer.toHexString(num).toUpperCase();
        return numStr;
    }

    class PlayTask extends AsyncTask<Object, Void, Void> {
        @Override
        protected Void doInBackground(Object[] params) {
            android.os.Process.setThreadPriority(10 + -1);

            while (!this.isCancelled()) {
                if (!isExportingOrImporting) {
                    generateTone();
                    audioOutput.write(samples, 0, samples.length);
                }
            }
            return null;
        }
    }

    private void generateTone() {

        for (int b=0;b<numABuffsInBuff;b++) {
            mHBEngine.gen(aSamlples);

            for (int a=0;a<upscaledSampleBuffSize;a++) {
                final int s = aSamlples[(int) (a/aToRealFreqRatio)] << 4;
                samples[a+b*upscaledSampleBuffSize] = (short) ((s - 120) << 8);
            }
        }


    }

    class AudioOutput {
        private AudioTrack mAudioTrack;
        private int mFrameRate;

        public void Start(int sampleRate, int bufferSize) {
            Stop();
            mFrameRate = sampleRate;
            mAudioTrack = createAudioTrack(bufferSize);

            mAudioTrack.play();
        }

        private AudioTrack createAudioTrack(int buffsize) {
            return new AudioTrack(AudioManager.STREAM_MUSIC,
                    mFrameRate, AudioFormat.CHANNEL_OUT_MONO,
                    AudioFormat.ENCODING_PCM_16BIT, buffsize,
                    AudioTrack.MODE_STREAM);
        }

        public int write(short[] buffer, int offset, int length) {
            return mAudioTrack.write(buffer, offset, length);
        }

        public void Stop() {
            if (mAudioTrack != null) {
                mAudioTrack.stop();
                mAudioTrack.release();
                mAudioTrack = null;
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        closeAllWindows();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case PERMISSION_REQUEST_EXPORT:
                    doExport();
                    break;
                case PERMISSION_REQUEST_LOAD:
                    doLoad();
                    break;
                case PERMISSION_REQUEST_SAVE:
                    doSave();
                    break;
                case PERMISSION_REQUEST_BACKUP:
                    doBackup();
                    break;
                case PERMISSION_REQUEST_IMPORT_RMT_TRACK:
                    doImportRmtTrack();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!mIsInitialSongLoadingFinished) {
            mHBEngine.resetPlaybackPosition(true);
            mHBEngine.setSongPlaying(false);
            findViewById(R.id.patternRowHighlight).setVisibility(View.GONE);

            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                doLoad();
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSION_REQUEST_LOAD);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mIsInitialSongLoadingFinished) {
            mHBEngine.resetPlaybackPosition(true);
            mHBEngine.setSongPlaying(false);
            findViewById(R.id.patternRowHighlight).setVisibility(View.GONE);

            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                doSave();
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSION_REQUEST_SAVE);
            }
        }
    }

    private void doImportRmtTrack() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                isExportingOrImporting = true;
                try {
                    mHBEngine.importRmtTrack();
                } catch (IOException e) {
                    toast(e.getMessage());
                    e.printStackTrace();
                }
                isExportingOrImporting = false;
            }
        }).start();
    }

    private void doExport() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                isExportingOrImporting = true;
                try {
                    mHBEngine.export();
                } catch (IOException e) {
                    toast(e.getMessage());
                    e.printStackTrace();
                }
                isExportingOrImporting = false;
            }
        }).start();
    }

    private void doSave() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final String fileName = mHBEngine.saveSong(false);
                    toast(String.format("Song saved as %s", fileName));
                } catch (IOException e) {
                    toast(e.getMessage());
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void doBackup() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mHBEngine.saveSong(false);
                    final String backupFileName = mHBEngine.saveSong(true);
                    toast(String.format("Backup saved as %s", backupFileName));
                } catch (IOException e) {
                    toast(e.getMessage());
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void doLoad() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final String fileName = mHBEngine.loadSong();
                    toast(String.format("Song %s loaded", fileName));
                } catch (IOException e) {
                    toast(e.getMessage());
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    toast(e.getMessage());
                    e.printStackTrace();
                }

                mIsInitialSongLoadingFinished = true;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateSongDisplay();
                        mPatternAdapter.notifyDataSetChanged();
                        mSongPatternAdapter.notifyDataSetChanged();
                    }
                });
            }
        }).start();
    }

    private void toast(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void updateSongDisplay() {
        ((NumberIncDec) mSongLenghtView.getTag()).setNum(mHBEngine.getSong().length);
        ((NumberIncDec) mSongLoopView.getTag()).setNum(mHBEngine.getSong().loopPos);
    }
}
