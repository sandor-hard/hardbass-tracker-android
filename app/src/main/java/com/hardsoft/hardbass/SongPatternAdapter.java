package com.hardsoft.hardbass;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SongPatternAdapter extends RecyclerView.Adapter<SongPatternAdapter.ViewHolder> {

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView labelText;
        TextView numEditText;
        NumberIncDec numEdit;
        int position;

        public ViewHolder(View view) {
            super(view);
            labelText = (TextView) view.findViewById(R.id.numlabel);
            numEditText = (TextView) view.findViewById(R.id.numedit);
            numEdit = new NumberIncDec(numEditText) {
                @Override
                public boolean onInc(int newNum) {
                    final boolean result = newNum<HBEngine.MAX_PATTERN_COUNT;
                    if (result) {
                        mNumIncListener.onInc(position, newNum);
                    }
                    return result;
                }
            };
            numEditText.setTag(numEdit);

            numEditText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mNumIncListener.onClick(position, numEdit.getNum());
                }
            });

            numEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    mNumIncListener.onClick(position, numEdit.getNum());
                }
            });
        }
    }

    public interface NumIncListener {
        void onInc(int position, int newNum);
        void onClick(int position, int p);
    }

    private final HBEngine mHBEngine;
    private final LayoutInflater mLayoutInflater;
    private NumIncListener mNumIncListener;

    public SongPatternAdapter(HBEngine engine, LayoutInflater layoutInflater,
                              NumIncListener numIncListener) {
        mHBEngine = engine;
        mLayoutInflater = layoutInflater;
        mNumIncListener = numIncListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mLayoutInflater
                .inflate(R.layout.songpattern, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String numStr = (position < 0x10 ? "0" : "") + Integer.toHexString(position).toUpperCase();
        holder.labelText.setText(numStr);
        holder.position = position;
        holder.numEdit.setNum(mHBEngine.getSong().patternIndices[position]);
    }

    @Override
    public int getItemCount() {
        return mHBEngine.getSong().length;
    }
}