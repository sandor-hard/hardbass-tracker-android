package com.hardsoft.hardbass;

import java.io.Serializable;

public class LFO implements Serializable {

    private static final long serialVersionUID = -8988293770308443417L;

    public int depth = 0x3;
    public int delayFrames = 8;
    public int periodFrames = 8; //Must be >=3
    public boolean enabled = false;

    public LFO(boolean enabled, int depth, int delayFrames, int periodFrames) {
        this.depth = depth;
        this.delayFrames = delayFrames;
        this.periodFrames = periodFrames;
        this.enabled = enabled;
    }
}
