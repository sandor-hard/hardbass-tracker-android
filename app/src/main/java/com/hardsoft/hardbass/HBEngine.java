/*
    HARDBASS Tracker/Sound Engine
    to be used with HARDBASS Atari XL/XE as introduced in the REHARDEN demo

    Created by Sandor Teli / HARD (10/2016-11/2017)
 */

package com.hardsoft.hardbass;

import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

public class HBEngine {

    static final String TAG = "HBEngine";
    private static final String HARDBASS_FINGERPRINT = "HBASSA8";
    private static final int HARDBASS_VERSION = 0x01;

    public static final int SAMPLE_RATE = 1950;
    public static final int BUFF_SIZE = 39;
    public static final int OCTAVES = 6;

    static final double NOTE_MULT = Math.pow(2.0d, 1.0d/12.0d);
    static final double A4 = 440.0d;
    static final double HZ_INC = 65536.0d / (double) SAMPLE_RATE;

    static final int MAX_SONG_LENGTH = 0x100;
    static final int MAX_PATTERN_COUNT = 0x40;
    static final int MAX_INSTRUMENT_COUNT = 0x40;
    static final int MIN_PORTAMENTO_STAGE_FRAMES = 3;

    private static final byte EXPORT_COMMAND_SKIP_FRAMES = 0;
    private static final byte EXPORT_COMMAND_NOTE_CHANGE = 1;
    private static final byte EXPORT_COMMAND_FLAGS_CHANGE = 2;
    private static final byte EXPORT_COMMAND_INSTRUMENT_CHANGE = 0x3;
    private static final byte EXPORT_COMMAND_CUTOFF_CHANGE = 0x4;
    private static final byte EXPORT_COMMAND_PW_CHANGE = 0x5;
    private static final byte EXPORT_COMMAND_PORTAMENTO_STAGE_START_FROM_FREQ = 0x6;
    private static final byte EXPORT_COMMAND_PORTAMENTO_END = 0x7;

    private static final int EXPORT_COMMAND_SKIPFRAMES_FLAG = 1<<7;

    //Instrument flags
    private static final int EXPORT_COMMAND_SOUND_FLAG = 1<<7;
    private static final int EXPORT_COMMAND_VIBRATO_FLAG = 1<<6;
    private static final int EXPORT_COMMAND_CUTOFF_LFO_FLAG = 1<<5;
    private static final int EXPORT_COMMAND_PW_LFO_FLAG = 1<<4;

    private int histBck = 0;

    private int[] divOld = new int[0x100];
    private int cutoffTblAddr = 0;
    private int impulseHiM1 = 0;
    private int inc = 0x00;
    private int pw = 0x00;
    private int accu = 0;

    private Song song = new Song();

    private PatternCommands[] songPatternCommands = null;

    private int songCursor = -1;
    private Pattern pattern = null;
    private PatternCommands patternCommands = null;
    private int patternCursor = -1;
    private int patternCursorSubStep = -1;
    private PatternCommands.PortamentoCommand portamentoCommand = null;

    private boolean isSongPlaying = false;

    private Instrument instrument = null;
    
    private int baseInc = 0;

    private boolean isAudible = false;

    private int holdGateForFrames = -1;

    private int portamentoStage = -1;
    private int portamentoStageFramesRemaining = 0;

    private int vibratoDepth = 0;
    private int vibratoDelayCountDown = 0;
    private int vibratoInc = 0;
    private int vibratoValue = 0;
    private boolean isVibratoGoingUp = false;
    private boolean isVibratoActive = false;

    private int baseCutoff = 0;

    private int cutoffLFODepth = 0;
    private int cutoffLFODelayCountDown = 0;
    private int cutoffLFOInc = 0;
    private int cutoffLFOValue = 0;
    private boolean isCutoffLFOGoingUp = false;
    private boolean isCutoffLFOActive = false;

    private int basePw = 0;

    private int pwLFODepth = 0;
    private int pwLFODelayCountDown = 0;
    private int pwLFOInc = 0;
    private int pwLFOValue = 0;
    private boolean isPwLFOGoingUp = false;
    private boolean isPwLFOActive = false;

    private int trackerFrame = 0;

    //export
    private int exportLastCommandFrame = 0;
    private boolean isExportFirstVibratoFrame = false;
    private boolean isExportFirstCutoffLFOFrame = false;
    private boolean isExportFirstPWLFOFrame = false;
    private int exportNextNoteIndex = 0;
    private Map<Integer, Integer> exportNoteTranslatedIndices = null;
    private ArrayList<ExportCommands> songExportCommands = null;
    private ExportCommands exportCommands = null;
    private int exportFlags = 0;
    private boolean exportFlagsUpdated = false;

    public HBEngine() {
        final int range = 240;
        final int margin = (255-range) >> 1;
        final int step = range >> 4;
        for (int cutoffTblIndex=0;cutoffTblIndex<16;cutoffTblIndex++) {
            final int target = margin + cutoffTblIndex*step;
            initScaleArr(cutoffTblIndex<<4, target);
        }

        setCutoff(2);
        pw = 0x80;

        resetPlaybackPosition(true);
    }

    public void setCutoff(int cutoff) {
        cutoffTblAddr = (15-cutoff)<<4;
        impulseHiM1 = (15-divOld[cutoffTblAddr|0x0f])-1; //Mimicking the always set C flag in ASM
    }

    private void initScaleArr(int addr, int target) {
        int accu = 0;
        int val = 0;
        for (int a=0;a<0x10;a++) {
            divOld[addr|a] = val;
            accu+= target;
            if (accu>=0x100) {
                val++;
                accu-= 0x100;
            }
        }
    }

    public void gen(int[] out) {
        if (isSongPlaying) {
            doTracker(false);
        }

        int y = histBck;

        for (int x=0;x<BUFF_SIZE;x++) {
            if (isSongPlaying) {
                accu += inc;
            }
            if (accu>=0x10000) {
                accu-= 0x10000;
            }

            final int accuHi = accu>>8;
            final int input = accuHi>=pw?(impulseHiM1+1):0; //Mimicking the always set C flag in ASM

            y = divOld[cutoffTblAddr | y] + input;

            out[x] = y;
        }

        histBck = y;
   }

    public int getTrackerStepCursor() {
        return patternCursor;
    }

    private boolean doTracker(boolean export) {
        boolean didLoopBack = false;

        if (patternCursorSubStep==0) {
            final PatternRow row = pattern.getRow(patternCursor);

            final ArrayList<PatternCommands.Command> rowCommands =
                    patternCommands.getRowCommands(patternCursor);

            if (rowCommands!=null) {
                for (int c = 0; c < rowCommands.size(); c++) {
                    final PatternCommands.Command command = rowCommands.get(c);

                    if (command instanceof PatternCommands.InstrumentChangeCommand) {
                        final PatternCommands.InstrumentChangeCommand instrumentChangeCommand =
                                (PatternCommands.InstrumentChangeCommand) command;
                        instrument = instrumentChangeCommand.instrument;
                        isVibratoActive = false;
                        isCutoffLFOActive = false;
                        isPwLFOActive = false;
                        holdGateForFrames = -1;

                        pwLFODepth = instrument.getPwLFODepth();
                        setPwLFOInc();

                        cutoffLFODepth = instrument.getCutoffLFODepth();
                        setCutoffLFOInc();

                        if (export) {
                            if (isAudible) {
                                updateExportFlags(EXPORT_COMMAND_SOUND_FLAG, false);
                            }
                            exportCommand(EXPORT_COMMAND_INSTRUMENT_CHANGE,
                                    (byte) pwLFODepth, (byte) pwLFOInc,
                                    (byte) cutoffLFODepth, (byte) cutoffLFOInc);
                        }

                        isAudible = false;
                        inc = 0;
                    } else if (command instanceof PatternCommands.NoteCommand) {
                        final PatternCommands.NoteCommand noteCommand =
                                (PatternCommands.NoteCommand) command;
                        baseInc = noteCommand.noteInc;
                        vibratoDepth = noteCommand.vibratoDepth;
                        setVibratoInc();

                        if (export) {
                            final Integer noteKey = new Integer(noteCommand.note);
                            if (!exportNoteTranslatedIndices.containsKey(noteKey)) {
                                exportNoteTranslatedIndices.put(noteKey, new Integer(exportNextNoteIndex));
                                exportNextNoteIndex++;
                            }

                            if (instrument.getVibratoEnabled()) {
                                exportCommand(EXPORT_COMMAND_NOTE_CHANGE,
                                        (byte) (exportNoteTranslatedIndices.get(noteKey).intValue()),
                                        (byte) (vibratoDepth & 0xff),
                                        (byte) (vibratoInc & 0xff));
                            }
                            else {
                                exportCommand(EXPORT_COMMAND_NOTE_CHANGE,
                                        (byte) (exportNoteTranslatedIndices.get(noteKey).intValue()));
                            }
                        }

                        if (!export) {
                            Log.d(TAG, String.format("New note, inc=0x%x, new baseInc=0x%x, vibDepth=0x%x, vibInc=0x%x (frame: 0x%x)", inc, baseInc, vibratoDepth, vibratoInc, trackerFrame));
                        }
                    } else if (command instanceof PatternCommands.PortamentoCommand) {
                        portamentoCommand = (PatternCommands.PortamentoCommand) command;
                        portamentoStage = -1;

                        if (export) {
                            //No need to export this directly
                            //as the individual stages will be sent as commands anyways
                        }

                    } else if (command instanceof PatternCommands.EnvelopeAttackCommand) {
                        isAudible = true;

                        vibratoDelayCountDown = instrument.getVibratoDelayFrames();
                        vibratoValue = 0;
                        isVibratoGoingUp = true;
                        isVibratoActive = instrument.getVibratoEnabled();

                        cutoffLFODelayCountDown = instrument.getCutoffLFODelayFrames();
                        cutoffLFOValue = 0;
                        isCutoffLFOGoingUp = true;
                        isCutoffLFOActive = instrument.getCutoffLFOEnabled();

                        pwLFODelayCountDown = instrument.getPwLFODelayFrames();
                        pwLFOValue = 0;
                        isPwLFOGoingUp = true;
                        isPwLFOActive = instrument.getPwLFOEnabled();

                        holdGateForFrames = instrument.getHoldGateForFrames();

                        if (export) {
                            updateExportFlags(EXPORT_COMMAND_SOUND_FLAG, true);

                            if (!isVibratoActive || vibratoDelayCountDown>0) {
                                updateExportFlags(EXPORT_COMMAND_VIBRATO_FLAG, false);
                            }
                            else {
                                isExportFirstVibratoFrame = isVibratoActive;
                            }
                            if (!isCutoffLFOActive || cutoffLFODelayCountDown>0) {
                                updateExportFlags(EXPORT_COMMAND_CUTOFF_LFO_FLAG, false);
                            }
                            else {
                                isExportFirstCutoffLFOFrame = isCutoffLFOActive;
                            }
                            if (!isPwLFOActive || pwLFODelayCountDown>0) {
                                updateExportFlags(EXPORT_COMMAND_PW_LFO_FLAG, false);
                            }
                            else {
                                isExportFirstPWLFOFrame = isPwLFOActive;
                            }
                        }
                    } else if (command instanceof PatternCommands.EnvelopeReleaseCommand) {
                        //We only respect release pattern commands if holdGateForFrames is not set
                        //as it overrides the track gate settings when set
                        if (instrument.getHoldGateForFrames()<=0) {
                            isAudible = false;
                            if (export) {
                                updateExportFlags(EXPORT_COMMAND_SOUND_FLAG, false);
                            }
                            inc = 0;
                        }
                    } else if (command instanceof PatternCommands.CutoffChangeCommand) {
                        final PatternCommands.CutoffChangeCommand cutoffChangeCommand =
                                (PatternCommands.CutoffChangeCommand) command;
                        baseCutoff = cutoffChangeCommand.cutoff;
                        if (export) {
                            exportCommand(EXPORT_COMMAND_CUTOFF_CHANGE, (byte) baseCutoff);
                        }
                    } else if (command instanceof PatternCommands.PwChangeCommand) {
                        final PatternCommands.PwChangeCommand pwChangeCommand =
                                (PatternCommands.PwChangeCommand) command;
                        basePw = pwChangeCommand.pw;
                        if (export) {
                            exportCommand(EXPORT_COMMAND_PW_CHANGE, (byte) basePw);
                        }
                    }
                }
            }
        }

        //-----------------------------------------------------------------------

        if (holdGateForFrames>=0) {
            holdGateForFrames--;
            if (holdGateForFrames==-1) {
                isAudible = false;
                if (export) {
                    updateExportFlags(EXPORT_COMMAND_SOUND_FLAG, false);
                }
                inc = 0;
            }
        }

        if (instrument!=null) {
            if (portamentoCommand != null) {
                //Skip the first frame so that it can be the original freq without incrementing
                if (portamentoStage >= 0 && portamentoStageFramesRemaining > 0) {
                    //Portamento running with remaining frame in current stage

                    baseInc += portamentoCommand.portamentoStages[portamentoStage].addToInc;
                    portamentoStageFramesRemaining--;

                    if (!export) {
                        Log.d(TAG, String.format("Upd port, stage=%d, inc=0x%x, baseInc=0x%x, framesRem=%d (frame: 0x%x)", portamentoStage, inc, baseInc, portamentoStageFramesRemaining, trackerFrame));
                    }
                } else {
                    //Portamento not initialized yet or current stage exhausted

                    portamentoStage++;
                    if (portamentoStage < portamentoCommand.portamentoStages.length) {
                        final PatternCommands.PortamentoCommand.PortamentoStage ps =
                                portamentoCommand.portamentoStages[this.portamentoStage];
                        baseInc = ps.startInc;
                        portamentoStageFramesRemaining = ps.repeateForFrames - 1;

                        boolean didVibratoChange = false;

                        if (portamentoStage==0) {
                            final int vibratoDepthOld = vibratoDepth;
                            final int vibratoIncOld = vibratoInc;

                            vibratoDepth = ps.vibratoDepth;
                            setVibratoInc();

                            didVibratoChange = vibratoDepthOld!=vibratoDepth || vibratoIncOld!=vibratoInc;

                            if (!export) {
                                Log.d(TAG, String.format("Init port vibrato vibDepth=0x%x, vibInc=0x%x (frame: 0x%x)", vibratoDepth, vibratoInc, trackerFrame));
                            }
                        }

                        if (export) {
                            if (portamentoStage==0 && didVibratoChange) {
                                //sending special note command with note value 0xff indicating we're only setting the vibrato
                                exportCommand(EXPORT_COMMAND_NOTE_CHANGE,
                                        (byte) 0xff,
                                        (byte) (vibratoDepth&0xff),
                                        (byte) (vibratoInc&0xff));
                            }

                            exportCommand(EXPORT_COMMAND_PORTAMENTO_STAGE_START_FROM_FREQ,
                                    (byte) (baseInc & 0xff), (byte) (baseInc >> 8),
                                    (byte) (ps.addToInc & 0xff),
                                    (byte) (ps.addToInc >> 8));
                        }
                        else {
                            Log.d(TAG, String.format("Init port, stage=%d, inc=0x%x, baseInc=0x%x, framesRem=%d (frame: 0x%x)", portamentoStage, inc, baseInc, portamentoStageFramesRemaining, trackerFrame));
                        }
                    } else {
                        portamentoCommand = null;

                        if (export) {
                            exportCommand(EXPORT_COMMAND_PORTAMENTO_END);
                        }
                        else {
                            Log.d(TAG, String.format("Ending port (frame: 0x%x)", trackerFrame));
                        }
                    }
                }
            }

            if (isAudible) {
                if (isVibratoActive) {
                    if (vibratoDelayCountDown > 0) {
                        if (!export) {
                            Log.d(TAG, String.format("vibratoDelayCountDown = %d (frame: 0x%x)", vibratoDelayCountDown, trackerFrame));
                        }
                        vibratoDelayCountDown--;

                        if (export) {
                            if (vibratoDelayCountDown == 0) {
                                isExportFirstVibratoFrame = true;
                            }
                        }
                    } else {
                        if (isExportFirstVibratoFrame) {
                            isExportFirstVibratoFrame = false;
                            updateExportFlags(EXPORT_COMMAND_VIBRATO_FLAG, true);
                        }

                        if (isVibratoGoingUp) {
                            vibratoValue += vibratoInc;
                            if (vibratoValue >= vibratoDepth) {
                                isVibratoGoingUp = false;
                                vibratoValue = vibratoDepth;
                            }
                        } else {
                            vibratoValue -= vibratoInc;
                            if (vibratoValue <= -vibratoDepth) {
                                isVibratoGoingUp = true;
                                vibratoValue = -vibratoDepth;
                            }
                        }
                    }
                }
            }

            if (isCutoffLFOActive) {
                if (cutoffLFODelayCountDown > 0) {
                    if (!export) {
                        Log.d(TAG, String.format("cutoffLFODelayCountDown = %d (frame: 0x%x)", cutoffLFODelayCountDown, trackerFrame));
                    }
                    cutoffLFODelayCountDown--;

                    if (export) {
                        if (cutoffLFODelayCountDown == 0) {
                            isExportFirstCutoffLFOFrame = true;
                        }
                    }
                } else {
                    if (isExportFirstCutoffLFOFrame) {
                        isExportFirstCutoffLFOFrame = false;
                        updateExportFlags(EXPORT_COMMAND_CUTOFF_LFO_FLAG, true);
                    }

                    if (isCutoffLFOGoingUp) {
                        cutoffLFOValue += cutoffLFOInc;
                        if (cutoffLFOValue >= cutoffLFODepth) {
                            isCutoffLFOGoingUp = false;
                            cutoffLFOValue = cutoffLFODepth;
                        }
                    } else {
                        cutoffLFOValue -= cutoffLFOInc;
                        if (cutoffLFOValue <= -cutoffLFODepth) {
                            isCutoffLFOGoingUp = true;
                            cutoffLFOValue = -cutoffLFODepth;
                        }
                    }
                }
            }

            if (isPwLFOActive) {
                if (pwLFODelayCountDown > 0) {
                    if (!export) {
                        Log.d(TAG, String.format("pwLFODelayCountDown = %d (frame: 0x%x)", pwLFODelayCountDown, trackerFrame));
                    }
                    pwLFODelayCountDown--;

                    if (export) {
                        if (pwLFODelayCountDown == 0) {
                            isExportFirstPWLFOFrame = true;
                        }
                    }
                } else {
                    if (isExportFirstPWLFOFrame) {
                        isExportFirstPWLFOFrame = false;
                        updateExportFlags(EXPORT_COMMAND_PW_LFO_FLAG, true);
                    }

                    if (isPwLFOGoingUp) {
                        pwLFOValue += pwLFOInc;
                        if (pwLFOValue >= pwLFODepth) {
                            isPwLFOGoingUp = false;
                            pwLFOValue = pwLFODepth;
                        }
                    } else {
                        pwLFOValue -= pwLFOInc;
                        if (pwLFOValue <= -pwLFODepth) {
                            isPwLFOGoingUp = true;
                            pwLFOValue = -pwLFODepth;
                        }
                    }
                }
            }
        }

        if (isAudible) {
            inc = baseInc + vibratoValue;
            
            int newCutoff = baseCutoff + cutoffLFOValue;
            if (newCutoff<0) {
                newCutoff = 0;
            }
            else if (newCutoff>0xf) {
                newCutoff = 0xf;
            }
            setCutoff(newCutoff);

            int newPw = basePw + pwLFOValue;
            if (newPw<0) {
                newPw = 0;
            }
            else if (newPw>0xff) {
                newPw = 0xff;
            }
            pw = newPw;
        }

        if (export && exportFlagsUpdated) {
            exportCommand(EXPORT_COMMAND_FLAGS_CHANGE, (byte) exportFlags);
            exportFlagsUpdated = false;
        }

        //-----------------------------------------------------------------------

        trackerFrame++;

        patternCursorSubStep = (patternCursorSubStep+1)%song.framesPerStep;
        if (patternCursorSubStep==0) {
            patternCursor = (patternCursor+1)%song.patternLength;
            if (patternCursor==0) {
                //Load new pattern

                if (export) {
                    //Flush any accumulated skip frame command
                    //So that we start from a clean state at the new pattern's start
                    exportCommand();

                    //Writing out the pattern end command directly (=0x80=Skip zero frames)
                    writeExportCommand((byte) EXPORT_COMMAND_SKIPFRAMES_FLAG);
                    exportCommands.commitLastCommand();
                }

                final int prevSongCursor = songCursor;
                songCursor = getNextSongCursorPosition(songCursor);

                didLoopBack = songCursor<=prevSongCursor;

                pattern = song.patterns[song.patternIndices[songCursor]];

                patternCommands = songPatternCommands[songCursor];

                if (export) {
                    exportCommands.end();

                    if (!didLoopBack) {
                        exportCommands = new ExportCommands(songCursor);
                        songExportCommands.add(exportCommands);
                    } else {
                        exportCommands = null;
                    }
                }
            }
        }


        return didLoopBack;
    }

    private void updateExportFlags(int flag, boolean isOn) {
        if (isOn) {
            exportFlags|= flag;
        }
        else {
            exportFlags&= ~flag;
        }

        exportFlagsUpdated = true;
    }

    private void exportCommand(byte... b) {
        if (exportLastCommandFrame< trackerFrame) {
            int skipFames = trackerFrame -exportLastCommandFrame;
            exportLastCommandFrame = trackerFrame;

            while (skipFames>0xff) {
                writeExportCommand(EXPORT_COMMAND_SKIP_FRAMES, (byte) 0xff);
                exportCommands.commitLastCommand();
                skipFames-= 0xff;
            }

            if (skipFames<0x10 && exportCommands.hasLastCommand()) {
                exportCommands.addSkipFramesToLastCommand((byte) (EXPORT_COMMAND_SKIPFRAMES_FLAG|(skipFames<<3)));
                exportCommands.commitLastCommand();

                Log.d(TAG, String.format("^- skip %d", skipFames));
            }
            else {
                writeExportCommand(EXPORT_COMMAND_SKIP_FRAMES, (byte) skipFames);
                exportCommands.commitLastCommand();
            }
        }

        if (b!=null && b.length>0) {
            writeExportCommand(b);
        }
    }

    private void writeExportCommand(byte... b) {
        String s = "";
        for (byte bb : b) {
            s+= Integer.toHexString(bb<0?(int)bb+0x100:bb) + " ";
        }

        Log.d(TAG, "writeExportCommand: " + s);

        exportCommands.addCommand(b);
    }

    private void setVibratoInc() {
        vibratoInc = Math.min(0x3f,
                Math.round((float) vibratoDepth*4/(float) instrument.getVibratoPeriodFrames()));
    }

    private void setCutoffLFOInc() {
        cutoffLFOInc = Math.round((float) cutoffLFODepth*4/(float)instrument.getCutoffLFOPeriodFrames());
    }

    private void setPwLFOInc() {
        pwLFOInc = Math.round((float) pwLFODepth*4/(float)instrument.getPwLFOPeriodFrames());
    }

    public int getPatternLength() {
        return song.patternLength;
    }

    public Pattern getCurrentPattern() {
        return pattern;
    }

    public Pattern setCurrentPattern(int songPosition, int p) {
        pattern = song.patterns[p];
        songCursor = songPosition;
        return pattern;
    }

    public void setSongPlaying(boolean b) {
        isSongPlaying = b;
    }

    public void resetPlaybackPosition(boolean restartSong) {
        patternCursor = 0;
        patternCursorSubStep = 0;
        trackerFrame = 0;
        inc = 0;
        baseInc = 0;
        isAudible = false;

        if (restartSong || pattern==null) {
            songCursor = 0;
            pattern = song.patterns[song.patternIndices[songCursor]];
        }

        preProcessCommands();
        patternCommands = songPatternCommands[songCursor];
    }

    private int getNextSongCursorPosition(int currentSongCursor) {
        int result = currentSongCursor+1;
        if (result>=song.length) {
            result = song.loopPos;
        }
        return result;
    }

    private void preProcessCommands() {
        songPatternCommands = new PatternCommands[song.length];
        final PatternCommands.State state = new PatternCommands.State();
        for (int s = 0; s<song.length; s++) {
            final Pattern p = song.patterns[song.patternIndices[s]];
            final Pattern nextP = song.patterns[song.patternIndices[getNextSongCursorPosition(s)]];
            final PatternCommands previousCommands = s==0?null:songPatternCommands[s-1];
            final boolean forceIntrumentChangeCommand = s==songCursor || s==song.loopPos;
            songPatternCommands[s] = new PatternCommands(p, nextP, state, forceIntrumentChangeCommand, song.framesPerStep, song.noteIncs,
                    song.instruments, MIN_PORTAMENTO_STAGE_FRAMES);
        }
    }

    public void export() throws IOException {
        songExportCommands = new ArrayList<ExportCommands>();
        trackerFrame = 0;
        exportLastCommandFrame = 0;
        exportNextNoteIndex = 0;
        exportNoteTranslatedIndices = new HashMap<Integer, Integer>();
        exportFlags = 0;
        exportFlagsUpdated = false;
        exportCommands = new ExportCommands(0);
        songExportCommands.add(exportCommands);

        //TODO: Put all session varibles into an object and create a new object each time we export/reset song
        vibratoInc = 0;
        vibratoDepth = 0;

        isAudible = false;
        while (!doTracker(true)) {
            //Exporting
        }

        final File file = new File("/sdcard/export.hardbass");
        file.delete();

        FileOutputStream exportStream = null;

        try {
            exportStream = new FileOutputStream(file);

            int headerSize = 0;

            final byte[] fingerPrint = HARDBASS_FINGERPRINT.getBytes("US-ASCII");
            exportStream.write(fingerPrint);
            headerSize+= fingerPrint.length;
            exportStream.write((byte) HARDBASS_VERSION);
            headerSize++;

            if (song.loopPos >=0) {
                exportStream.write((byte) song.loopPos*2 + headerSize + 2 + exportNextNoteIndex*2);
            }
            else {
                exportStream.write((byte) 0xff);
            }
            headerSize++;

            exportStream.write((byte) exportNextNoteIndex); //note count
            headerSize++;

            final byte[] noteIncsLo = new byte[exportNextNoteIndex];
            final byte[] noteIncsHi = new byte[exportNextNoteIndex];
            for (Integer noteKey : exportNoteTranslatedIndices.keySet()) {
                final int translatedIndex = exportNoteTranslatedIndices.get(noteKey).intValue();
                noteIncsLo[translatedIndex] = (byte) (song.noteIncs[noteKey.intValue()]&0xff);
                noteIncsHi[translatedIndex] = (byte) (song.noteIncs[noteKey.intValue()]>>8);
            }

            exportStream.write(noteIncsLo);
            exportStream.write(noteIncsHi);

            headerSize+= exportNextNoteIndex*2;

            int currAddr = headerSize+songExportCommands.size()*2;

            final HashMap<String, ExportCommands> commandMap = new HashMap<String, ExportCommands>();
            final ArrayList<ExportCommands> doNotWriteList = new ArrayList<ExportCommands>();

            for (ExportCommands exportCommands : songExportCommands) {
                final String checksum = exportCommands.getChecksum();
                if (!commandMap.containsKey(checksum)) {
                    commandMap.put(checksum, exportCommands);
                    exportCommands.setAddressInExport(currAddr);
                    currAddr+= exportCommands.getBytes().length;
                }
                else {
                    doNotWriteList.add(exportCommands);
                }

                final ExportCommands uniqueCommands = commandMap.get(checksum);
                final int uniqueCommandsAddr = uniqueCommands.getAddressInExport();

                exportStream.write((byte) (uniqueCommandsAddr&0xff));
                exportStream.write((byte) (uniqueCommandsAddr>>8));
            }

            for (ExportCommands exportCommands : songExportCommands) {
                if (!doNotWriteList.contains(exportCommands)) {
                    final String checksum = exportCommands.getChecksum();
                    final ExportCommands uniqueCommands = commandMap.get(checksum);
                    exportStream.write(uniqueCommands.getBytes());
                }
            }
        } finally {
            if (exportStream!=null) {
                try {
                    exportStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String saveSong(boolean isBackup) throws IOException {
        final String fileName = isBackup?
                String.format("/sdcard/backup-%d.hbass", System.currentTimeMillis()):
                "/sdcard/song1.hbass";
        final File file = new File(fileName);
        file.delete();

        FileOutputStream saveStream = null;
        ObjectOutputStream objectOutputStream = null;

        try {
            saveStream = new FileOutputStream(file);
            objectOutputStream = new ObjectOutputStream(saveStream);
            objectOutputStream.writeObject(song);
        } finally {
            if (saveStream!=null) {
                try {
                    saveStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return fileName;
    }

    public String loadSong() throws IOException, ClassNotFoundException {
        final String fileName = "/sdcard/song1.hbass";
        final File file = new File(fileName);

        FileInputStream loadStream = null;
        ObjectInputStream objectInputStream = null;

        try {
            loadStream = new FileInputStream(file);
            objectInputStream = new ObjectInputStream(loadStream);
            song = (Song) objectInputStream.readObject();
            resetPlaybackPosition(true);
        } finally {
            if (loadStream!=null) {
                try {
                    loadStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return fileName;
    }

    public Song getSong() {
        return song;
    }

    public void importRmtTrack() throws IOException {
        final Properties settings = new Properties();
        settings.load(new FileInputStream("/sdcard/hardbassimport/import.properties"));

        final Scanner s = new Scanner(new File("/sdcard/hardbassimport/0.txt"));
        final ArrayList<String> lines = new ArrayList<String>();
        while (s.hasNextLine()){
            final String l = s.nextLine();
            if (!TextUtils.isEmpty(l)) {
                lines.add(l);
            }
        }
        s.close();

        final int patternLength = pattern.getLength();
        if (lines.size()==patternLength+2) {

            final int sourceInstr = getIntProp(settings, "sourceInstr");
            final int targetInstr = getIntProp(settings, "targetInstr");
            final int transpose = getIntProp(settings, "transpose");
            final int startCutoff = getIntProp(settings, "startCutoff");
            final int endCutoff = getIntProp(settings, "endCutoff");
            final int startPW = getIntProp(settings, "startPW");
            final int endPW = getIntProp(settings, "endPW");
            final int pwStep = getIntProp(settings, "pwStep");
            final int holdGateForSteps = getIntProp(settings, "holdGateForSteps");

            final boolean isPwRunning = startPW>-1 && endPW>-1 && startPW!=endPW && pwStep>0;
            final boolean isCutoffRunning = startCutoff>-1 && endCutoff>-1 && startCutoff!=endCutoff;
            final int cutoffRange = isCutoffRunning?Math.abs(endCutoff-startCutoff)+1:0;
            final int cutoffStepSign = isCutoffRunning?(endCutoff>startCutoff?1:-1):0;

            final List<String> notes = Arrays.asList(PatternRow.NOTE_NAMES);

            int holdGate = 0;
            int[] runningCutoffValues = null;
            int noteStep = 0;

            for (int r = 0; r < patternLength; r++) {
                final PatternRow row = pattern.getRow(r);

                final String l = lines.get(2+r);

                Log.d(TAG, String.format("Processing row:%d line=%s", r, l));

                final String[] larr = l.split(" ");
                final String noteStr = larr[0];
                final String instrStr = larr[1];

                Log.d(TAG, String.format("Processing row:%d noteStr=%s instrStr=%s", r, noteStr, instrStr));

                int note = -1;
                int gateCmd = -1;
                int cutoff = -1;
                int pw = -1;
                int instrument = -1;

                final boolean newHbassNote = isNewHbassNote(sourceInstr, noteStr, instrStr);

                if (newHbassNote) {
                    final String notePart = noteStr.substring(0, 2);
                    final int octPart = Integer.valueOf(noteStr.substring(2));
                    final int notePartIndex = notes.indexOf(notePart);
                    note = notePartIndex + octPart * 12 + transpose;
                    gateCmd = 0;
                    holdGate = holdGateForSteps;
                    noteStep = 0;

                    int endRow = patternLength-1;
                    for (int rr=r+1; rr<patternLength; rr++) {
                        final String ll = lines.get(2+rr);
                        final String[] llarr = ll.split(" ");
                        final String noteStr2 = llarr[0];
                        final String instrStr2 = llarr[1];
                        final boolean newHbassNote2 = isNewHbassNote(sourceInstr, noteStr2, instrStr2);
                        if (newHbassNote2) {
                            endRow = rr-1;
                            break;
                        }
                    }

                    int noteLength = Math.min(holdGateForSteps, endRow-r+1);

                    if (isCutoffRunning && noteLength>1) {
                        runningCutoffValues = new int[noteLength];
                        if (noteLength<=cutoffRange) {
                            int cfix = 0;
                            int cf=startCutoff;
                            while (cfix<noteLength) {
                                runningCutoffValues[cfix] = cf;
                                cfix++;
                                cf+=cutoffStepSign;
                            }
                        }
                        else {
                            for (int cfix=0;cfix<noteLength;cfix++) {
                                runningCutoffValues[cfix] = -1;
                            }

                            runningCutoffValues[0] = startCutoff;
                            int cfix = noteLength-1;
                            int cf=endCutoff;
                            while (cfix>(noteLength-cutoffRange)) {
                                runningCutoffValues[cfix] = cf;
                                cfix--;
                                cf-=cutoffStepSign;
                            }
                        }
                    }
                    else {
                        runningCutoffValues = null;
                    }

                    if (r==0) {
                        instrument = targetInstr;
                        if (startPW>-1) {
                            pw = startPW;
                        }
                        if (startCutoff>-1) {
                            cutoff = startCutoff;
                        }
                    }

                }

                if (holdGate>0) {
                    gateCmd = 0;
                    holdGate--;
                }

                if (runningCutoffValues!=null && noteStep>=0 && noteStep<runningCutoffValues.length) {
                    final int runningCutoffValue = runningCutoffValues[noteStep];
                    cutoff = runningCutoffValue;
                }

                noteStep++;
                row.setNote(note);
                row.setGateCmd(gateCmd);
                row.setCutOff(cutoff);
                row.setPW(pw);
                row.setInstrument(instrument);

            }
        }
        else {
            throw new IOException("Pattern length mismatch");
        }

    }

    private boolean isNewHbassNote(int sourceInstr, String noteStr, String instrStr) {
        boolean result = false;

        if (!TextUtils.isEmpty(noteStr) && !TextUtils.isEmpty(instrStr) &&
                !noteStr.equals("---") && !instrStr.equals("--")) {
            final int instr = Integer.parseInt(instrStr, 16);
            if (instr==sourceInstr) {
                result = true;
            }
        }

        return result;
    }

    private int getIntProp(Properties settings, String key) throws IOException {
        final String s = settings.getProperty(key);
        if (TextUtils.isEmpty(s)) {
            throw new IOException("Settings error - empty setting");
        }

        int result = -1;

        try {
            result = Integer.valueOf(s);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            throw new IOException("Settings error - conversion failed");
        }

        return result;
    }

    public int getSongCursor() {
        return songCursor;
    }
}
