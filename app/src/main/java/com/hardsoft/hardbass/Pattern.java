package com.hardsoft.hardbass;

import java.io.Serializable;

public class Pattern implements Serializable {

    private static final long serialVersionUID = -1252388207621752970L;

    private final int mIndex;
    private final int mLength;
    private final PatternRow[] mRows;

    public Pattern(int index, int length) {
        mIndex = index;
        mLength = length;
        mRows = new PatternRow[length];
        for (int r=0;r<length;r++) {
            mRows[r] = new PatternRow(index, r);
        }
    }

    public PatternRow getRow(int rowIndex) {
        return mRows[rowIndex];
    }

    public int getLength() {
        return mLength;
    }

    public int getIndex() {
        return mIndex;
    }

}
