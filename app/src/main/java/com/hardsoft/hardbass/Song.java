package com.hardsoft.hardbass;

import android.util.Log;

import java.io.Serializable;

public class Song implements Serializable {

    private static final long serialVersionUID = -4280006732879480247L;

    final int[] noteIncs = new int[HBEngine.OCTAVES*12];

    int patternLength = 64;
    int framesPerStep = 3;

    Pattern[] patterns = new Pattern[HBEngine.MAX_PATTERN_COUNT];
    int[] patternIndices = new int[HBEngine.MAX_SONG_LENGTH];
    int length = 1;
    int loopPos = 0;

    Instrument[] instruments = new Instrument[HBEngine.MAX_INSTRUMENT_COUNT];

    public Song() {
        calcNoteTable();
        patterns[0] = new Pattern(0, patternLength);
        instruments[0] = new Instrument(0);
    }

    private void calcNoteTable() {
        final double[] oct4 = new double[12];
        oct4[9] = HBEngine.A4;
        for (int a=8;a>=0;a--) {
            oct4[a] = oct4[a+1]/HBEngine.NOTE_MULT;
        }
        for (int a=10;a<12;a++) {
            oct4[a] = oct4[a-1]*HBEngine.NOTE_MULT;
        }

        for (int o=0;o<HBEngine.OCTAVES;o++) {
            final double ratio = Math.pow(2, o-4);
            Log.d(HBEngine.TAG, "------------------------------------");
            for (int a=0;a<12;a++) {
                final double hz = oct4[a] * ratio;
                int inc = (int) Math.round(hz * HBEngine.HZ_INC);
                if (inc>0xffff) {
                    inc = 0xffff;
                }
                noteIncs[o*12+a] = inc;

                Log.d(HBEngine.TAG, String.format("oct%d[%d]=%f Hz (inc=$%s)", o, a, hz,
                        Integer.toHexString(inc)));
            }
        }
    }

    public void makePatternAvailable(int p) {
        if (patterns[p]==null) {
            patterns[p] = new Pattern(p, patternLength);
        }
    }

    public void makeInstrumentAvailable(int i) {
        if (instruments[i]==null) {
            instruments[i] = new Instrument(i);
        }
    }

    public void fixLoop() {
        if (loopPos>=length) {
            loopPos = length-1;
        }
    }

    public void setLength(int newNum) {
        length = newNum;
        fixLoop();
    }
}
