package com.hardsoft.hardbass;

import java.util.ArrayList;
import java.util.HashMap;

public class PatternCommands {

    static class State {
        boolean gate = false;

        int note = -1;
        int instrument = -1;
        int cutoff = 0;
        int pw = 0;
        int remainingPortamentoSteps = 0;
    }

    static class InstrumentChangeCommand extends Command {
        final Instrument instrument;

        public InstrumentChangeCommand(Instrument instrument) {
            this.instrument = instrument;
        }
    }

    static class CutoffChangeCommand extends Command {

        final int cutoff;

        public CutoffChangeCommand(int cutoff) {
            this.cutoff = cutoff;
        }
    }

    static class PwChangeCommand extends Command {

        final int pw;

        public PwChangeCommand(int pw) {
            this.pw = pw;
        }
    }

    static class EnvelopeAttackCommand extends Command {
    }

    static class EnvelopeReleaseCommand extends Command {
    }

    static class NoteCommand extends Command {
        final int note;
        final int noteInc;
        final int vibratoDepth;

        public NoteCommand(int note, int noteInc, int vibratoDepth) {
            this.note = note;
            this.noteInc = noteInc;
            this.vibratoDepth = vibratoDepth;
        }
    }

    static class PortamentoCommand extends Command {

        static class PortamentoStage {
            final int startInc;
            final int addToInc;
            final int repeateForFrames;
            final int vibratoDepth;

            public PortamentoStage(int startInc, int addToInc, int repeateForFrames,
                                   int vibratoDepth) {
                this.startInc = startInc;
                this.addToInc = addToInc;
                this.repeateForFrames = repeateForFrames;
                this.vibratoDepth = vibratoDepth;
            }
        }

        final PortamentoStage[] portamentoStages;

        public PortamentoCommand(int stageCount) {
            portamentoStages = new PortamentoStage[stageCount];
        }

    }

    static class Command {
    }

    private final Pattern mPattern;
    private final HashMap<Integer, ArrayList<Command>> mRowCommands =
            new HashMap<Integer, ArrayList<Command>>();


    public PatternCommands(Pattern pattern, Pattern nextPatternInSong, State state,
                           boolean forceIntrumentChangeCommand,
                           int framesPerStep, int[] noteIncs, Instrument[] instruments,
                           int minPortamentoStageFrames) {
        mPattern = pattern;

        final int patternLength = pattern.getLength();
        for (int r = 0; r<patternLength; r++) {
            final PatternRow row = pattern.getRow(r);

            final int note = row.getNote();
            final int gateCmd = row.getGateCmd();
            final int cutoff = row.getCutOff();
            final int pw = row.getPW();
            final int instrument = row.getInstrument();

            boolean justChangedIntrument = false;

            if ((instrument>=0 && state.instrument!=instrument) || (forceIntrumentChangeCommand&&r==0)) {
                if (instrument>=0) {
                    state.instrument = instrument;
                }
                if (state.instrument>=0) {
                    putCommand(r, new InstrumentChangeCommand(instruments[state.instrument]));
                    justChangedIntrument = true;
                }
            }

            if (note>=0) {
                state.note = note;
            }

            if (cutoff>=0) {
                state.cutoff = cutoff;
                putCommand(r, new CutoffChangeCommand(cutoff));
            }

            if (pw>=0) {
                state.pw = pw;
                putCommand(r, new PwChangeCommand(pw));
            }

            if (state.note>=0 && state.instrument>=0) {
                //Consuming portamento so that we don't start another one while one is already running
                if (state.remainingPortamentoSteps > 0) {
                    state.remainingPortamentoSteps--;
                }

                if (gateCmd == PatternRow.GATE_CMD_GATE_OFF) {
                    //Gate off
                    if (state.gate) {
                        state.gate = false;
                        putCommand(r, new EnvelopeReleaseCommand());
                    }
                } else {
                    //Gate on command or portamento with note set in the row
                    //We consider both as gate on
                    //Portamento command without a note means just portamento, not gate
                    if (gateCmd == PatternRow.GATE_CMD_GATE_ON
                            || note>=0) {
                        //Gate on
                        if (!state.gate || instruments[state.instrument].getHoldGateForFrames()>0
                                || justChangedIntrument) {
                            //We send an attack command either if gate was off or
                            //if the current instrument overrides gate handling via holdGateForFrames
                            //of if we just changed instrument
                            putCommand(r, new EnvelopeAttackCommand());
                            state.gate = true;
                        }
                    }

                    if (gateCmd == PatternRow.GATE_CMD_GATE_ON_PORT) {
                        //Gate on & portamento
                        if (state.remainingPortamentoSteps == 0) {
                            final PortamentoCommand portamentoCommand =
                                    getPortamentoCommand(row, state, pattern, nextPatternInSong,
                                            framesPerStep, noteIncs, instruments, minPortamentoStageFrames);

                            putCommand(r, portamentoCommand);
                        }
                    }
                }

                //Note just set and portamento is not going
                if (note >= 0 && state.remainingPortamentoSteps == 0) {
                    final Command command = new NoteCommand(note, noteIncs[note],
                            getVibratoDepth(noteIncs[note], instruments[state.instrument]));
                    putCommand(r, command);
                }
            }
        }
    }

    private int getVibratoDepth(int noteInc, Instrument instrument) {
        final int result = (int) (noteInc * instrument.getVibratoDepthRangeMultiplier() - noteInc);
        return result<0xff?result:0xff;
    }

    private PortamentoCommand getPortamentoCommand(PatternRow row, State state,
                                                   Pattern pattern, Pattern nextPatternInSong,
                                                   int framesPerStep, int[] noteIncs,
                                                   Instrument[] instruments, int minPortamentoStageFrames) {
        PortamentoCommand result = null;
        boolean succ = false;

        boolean isNextPatternDifferent = pattern != nextPatternInSong;
        final Pattern[] patterns = new Pattern[isNextPatternDifferent?2:1];
        patterns[0] = pattern;
        if (isNextPatternDifferent) {
            patterns[1] = nextPatternInSong;
        }

        int lengthInSteps = 0;
        int targetNote = -1;
        int startRow = row.getIndex()+1;
        for (int p=0;p<patterns.length;p++) {
            for (int r=startRow;r<patterns[p].getLength();r++) {

                lengthInSteps++;

                final int note = patterns[p].getRow(r).getNote();
                if (note>=0) {
                    targetNote = note;
                    break;
                }
            }
            if (targetNote>=0) {
                break;
            }
            else {
                startRow = 0;
            }
        }

        succ = targetNote>=0 && state.note!=targetNote;

        if (succ) {
            state.remainingPortamentoSteps = lengthInSteps;
            final int lengthInFrames = lengthInSteps*framesPerStep;
            final int distanceInSeminotes = targetNote - state.note;

            final int portamentoStages =
                    (int) Math.ceil((float) lengthInFrames/ (float) minPortamentoStageFrames);

            result = new PortamentoCommand(portamentoStages);
            final float snStep = (float) distanceInSeminotes/ (float) portamentoStages;

            final float framesPerPortamentoStage =
                    Math.abs((float) lengthInFrames / (float) portamentoStages);

            int lastIntFrame = 0;

            for (int s = 0; s < portamentoStages; s++) {

                final float fineSnPos = state.note + snStep * s;
                final int sn = (int) fineSnPos;
                final float snFraction = fineSnPos - sn;
                final int fullIncBetweenNeighbors = (sn<(noteIncs.length-1))?
                        (noteIncs[sn+1] - noteIncs[sn]):0;
                final int incFraction = (int) (fullIncBetweenNeighbors * snFraction);
                final int startInc = noteIncs[sn] + incFraction;

                final float fineSnPosTarget = state.note + snStep * (s+1);
                final int snTarget = (int) fineSnPosTarget;
                final float snFractionTarget = fineSnPosTarget - snTarget;
                final int fullIncBetweenNeighborsTarget = (snTarget<(noteIncs.length-1))?
                        (noteIncs[snTarget+1] - noteIncs[snTarget]):0;
                final int incFractionTarget = (int) (fullIncBetweenNeighborsTarget * snFractionTarget);
                final int targetInc = noteIncs[snTarget] + incFractionTarget;

                final int intFrame = Math.round(framesPerPortamentoStage * (s + 1));
                final int repeatForFrames = intFrame - lastIntFrame;
                lastIntFrame = intFrame;

                final int addToInc = Math.round((targetInc - startInc) / (float) repeatForFrames);

                final PortamentoCommand.PortamentoStage stage =
                        new PortamentoCommand.PortamentoStage(startInc, addToInc, repeatForFrames,
                                getVibratoDepth(startInc, instruments[state.instrument]));

                result.portamentoStages[s] = stage;
            }
        }

        return result;
    }

    private void putCommand(int rowIndex, Command command) {
        if (!mRowCommands.containsKey(rowIndex)) {
            mRowCommands.put(rowIndex, new ArrayList<Command>());
        }

        final ArrayList<Command> commands = mRowCommands.get(rowIndex);
        commands.add(command);
    }

    public ArrayList<Command> getRowCommands(int rowIndex) {
        return mRowCommands.get(rowIndex);
    }

}
