package com.hardsoft.hardbass;

import java.io.Serializable;

public class PatternRow implements Serializable {

    private static final long serialVersionUID = 43325822135455455L;

    public static final String[] NOTE_NAMES = new String[]{
            "C-",
            "C#",
            "D-",
            "D#",
            "E-",
            "F-",
            "F#",
            "G-",
            "G#",
            "A-",
            "A#",
            "B-"
    };

    public static final int[] GATE_CMD_STRING_RES = new int[]{
        R.string.gate_cmd_empty, R.string.gate_cmd_gate_on, R.string.gate_cmd_port
    };

    public static final int GATE_CMD_GATE_OFF = -1;
    public static final int GATE_CMD_GATE_ON = 0;
    public static final int GATE_CMD_GATE_ON_PORT = 1;

    private final int mPatternIndex;
    private final int mIndex;

    private int mNote = -1;
    private int mInstrument = -1;
    private int mGateCmd = -1;
    private int mCutOff = -1;
    private int mPW = -1;

    public PatternRow(int patternIndex, int index) {
        mPatternIndex = patternIndex;
        mIndex = index;
    }

    public String getNoteString() {
        String result = null;

        if (mNote<0) {
            result = "---";
        }
        else {
            result = NOTE_NAMES[mNote%12]+String.valueOf(mNote/12);
        }

        return result;
    }

    public String getInstrumentString() {
        String result = null;

        if (mInstrument<0) {
            result = "--";
        }
        else {
            result = (mInstrument<0x10?"0":"")+Integer.toHexString(mInstrument);
        }

        return result;
    }

    public int getNote() {
        return mNote;
    }

    public void setNote(int n) {
        mNote = n;
    }

    public void setInstrument(int i) {
        mInstrument = i;
    }

    public int getIndex() {
        return mIndex;
    }

    public int getInstrument() {
        return mInstrument;
    }

    public int getGateCmdStringRes() {
        return GATE_CMD_STRING_RES[mGateCmd+1];
    }

    public int getGateCmd() {
        return mGateCmd;
    }

    public void setGateCmd(int gateCmd) {
        mGateCmd = gateCmd;
    }

    public void cycleGateCmd() {
        mGateCmd = ((mGateCmd+2)%GATE_CMD_STRING_RES.length)-1;
    }

    public void setCutOff(int cutOff) {
        mCutOff = cutOff;
    }

    public int getCutOff() {
        return mCutOff;
    }

    public String getCutOffString() {
        String result = null;

        if (mCutOff<0) {
            result = "-";
        }
        else {
            result = Integer.toHexString(mCutOff);
        }

        return result;
    }

    public void setPW(int pw) {
        mPW = pw;
    }

    public int getPW() {
        return mPW;
    }

    public String getPWString() {
        String result = null;

        if (mPW<0) {
            result = "--";
        }
        else {
            result = (mPW<0x10?"0":"")+Integer.toHexString(mPW);
        }

        return result;
    }
}
