package com.hardsoft.hardbass;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PatternAdapter extends BaseAdapter {

    public static final int PADDING_TRACK_ROWS = 16;

    private final HBEngine mHBEngine;
    private final LayoutInflater mLayoutInflater;
    private View.OnClickListener mPatternRowClickListener;

    public PatternAdapter(HBEngine engine, LayoutInflater layoutInflater,
                          View.OnClickListener patternRowClickListener) {
        mHBEngine = engine;
        mLayoutInflater = layoutInflater;
        mPatternRowClickListener = patternRowClickListener;
    }

    @Override
    public int getCount() {
        return mHBEngine.getPatternLength() + PADDING_TRACK_ROWS*2;
    }

    @Override
    public Object getItem(int i) {
        final Pattern pattern = mHBEngine.getCurrentPattern();
        return pattern==null?null:pattern.getRow(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final Pattern pattern = mHBEngine.getCurrentPattern();

        if (view==null) {
            view = mLayoutInflater.inflate(R.layout.pattern_row, viewGroup, false);
        }

        final TextView rowNum = (TextView) view.findViewById(R.id.patternRowNumber);
        final TextView note = (TextView) view.findViewById(R.id.patternRowNote);
        final TextView gateCmd = (TextView) view.findViewById(R.id.patternGateCmd);
        final TextView instr = (TextView) view.findViewById(R.id.patternRowInstr);
        final TextView cutOff = (TextView) view.findViewById(R.id.patternRowCutoff);
        final TextView pw = (TextView) view.findViewById(R.id.patternRowPW);

        if (pattern!=null && i>=PADDING_TRACK_ROWS && i<(PADDING_TRACK_ROWS+mHBEngine.getPatternLength())) {
            final int num = i - PADDING_TRACK_ROWS;
            final String rowNumStr = (num < 0x10 ? "0" : "") + Integer.toHexString(num).toUpperCase();
            rowNum.setText(rowNumStr);

            final PatternRow row = (PatternRow) getItem(i-PADDING_TRACK_ROWS);

            note.setText(row.getNoteString());
            gateCmd.setText(row.getGateCmdStringRes());
            instr.setText(row.getInstrumentString());
            cutOff.setText(row.getCutOffString());
            pw.setText(row.getPWString());

            note.setTag(row);
            note.setOnClickListener(mPatternRowClickListener);
            gateCmd.setTag(row);
            gateCmd.setOnClickListener(mPatternRowClickListener);
            rowNum.setOnClickListener(mPatternRowClickListener);
            cutOff.setTag(row);
            cutOff.setOnClickListener(mPatternRowClickListener);
            pw.setTag(row);
            pw.setOnClickListener(mPatternRowClickListener);

            instr.setTag(row);
            instr.setOnClickListener(mPatternRowClickListener);
        }
        else {
            rowNum.setText(null);
            note.setText(null);
            gateCmd.setText(null);
            instr.setText(null);
            cutOff.setText(null);
            pw.setText(null);

            note.setTag(null);
            note.setOnClickListener(null);
            gateCmd.setOnClickListener(null);
            gateCmd.setTag(null);
            rowNum.setOnClickListener(null);

            instr.setTag(null);
            instr.setOnClickListener(null);

            cutOff.setTag(null);
            cutOff.setOnClickListener(null);
            pw.setTag(null);
            pw.setOnClickListener(null);
        }

        return view;
    }

    public void updateRow(PatternRow row) {
        notifyDataSetChanged();
    }

}
