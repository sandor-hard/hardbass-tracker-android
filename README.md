# HARDBASS Tracker for Android

## What is this tool for?

HARDBASS Tracker for Android is a tool for composing basslines for the HARDBASS soft-synth introduced by the REHARDEN demo for Atari 8-bit computers (64K PAL XL/XE).
Its UI and source code are highly experimental aiming at quick results for what's needed for the REHARDEN demo.

## What does this repo include?

This repo includes the full source code of HARDBASS Tracker for Android in the form of an Android Studio v2.2.2 project.

There's no separate documentation written for this tool, please see the source code and the main HARDBASS documentation at:

- [HARDBASS soft-synth documentation](https://bitbucket.org/sandor-hard/reharden/src/master/HARDBASS.md)

This repo also includes a folder with some example files mentioned in the HARDBASS documentation:

- Example song file (original bassline from the REHARDEN demo): /sdcard/song1.hbass . You'd push this into the /sdcard/ root on the Android device so that the tracker loads it up when you first launch it.
- Example RMT file (original POKEY music from the REHARDEN demo): reharden.rmt . You'd keep / edit the RMT file on your PC.
- Example exported RMT track: /sdcard/hardbassimport/0.txt
- Example import settings: /sdcard/hardbassimport/import.properties . You'd push both of these into their destination folder on the Android device's /sdcard/ before importing notes from RMT into the HARDBASS Tracker.

Sandor Teli / HARD
